package net.minelink.ctplus.hook;

import me.clip.placeholderapi.external.EZPlaceholderHook;
import net.minelink.ctplus.CombatTagPlus;
import net.minelink.ctplus.Tag;
import org.bukkit.entity.Player;

public class PlaceholderAPIHook extends EZPlaceholderHook {

    private CombatTagPlus plugin;

    public PlaceholderAPIHook(CombatTagPlus plugin) {
        super(plugin, "combattagplus");
        this.plugin = plugin;
    }

    // Super clean coding, love this guy. Extremely easy to add stuff, that's how people should code.

    @Override
    public String onPlaceholderRequest(Player p, String params) {

        if (p == null)
            return null;

        if (params.equals("duration")) {
            if (plugin.getTagManager().isTagged(p.getUniqueId())) {
                // Get player tag
                Tag tag = plugin.getTagManager().getTag(p.getUniqueId());
                // Get time remaining in seconds
                long time = tag.getTagDuration();

                if (time >= 3600)
                    return time / 3600 + "h " + (time % 3600) / 60 + "m " + (time % 3600) % 60 + "s";
                else if (time >= 60)
                    return time / 60 + "m " + time % 60 + "s";
                else
                    return time + "s";
            } else
                return plugin.getSettings().getPAPINoDurationMessage();
        }

        return null;
    }
}
